FROM registry.gitlab.com/tr_devops/build_python_image/pypi_build:1.0.0
LABEL authors="Tobias Riazi <riazi.tobias@healthnow.org>"
LABEL description="Set Version - Python"

USER root

COPY requirements.txt .
RUN pip3 install -r requirements.txt
RUN rm requirements.txt

USER 1001
